(** tests for bert.ml **)

exception Test_failure;;

let do_tests suite = 
  let rec run_tests my_suite suc = 
    match my_suite with
      | [] -> (suc = List.length suite);
      | hd::tl ->
	  if hd () then run_tests tl (suc+1)
	  else run_tests tl suc 
  in
    run_tests suite 0;;

let rec decoding_test answers problem results=
  match answers with
    | [] -> 
	results;
    | hd::tl ->
	match (Bert.decode_binary problem) with 
	  | Bert.Ok( term, remain )->
	      if term = hd then decoding_test tl remain (results+1)
	      else raise Test_failure;
	  | _ ->
	      raise Test_failure;;

let rec encode_all list str ofs =
  match list with
    | [] ->  ofs;
    | hd::tl ->
	let nofs = Bert.encode_term hd str ofs in
	  encode_all tl str nofs;;

let _ = 
  let list = [ Bert.Int(774567); Bert.Float(23.43); Bert.Atom("HAHAHAHA");
	       Bert.String("HAHAHA"); Bert.Binary("HAHAHA") ] in
  let str = String.create 128 in
  let _ = encode_all list str 0 in
  let n = decoding_test list str 0 in
    Printf.printf "tests_bert: %d/%d passed.\n" n (List.length list) ;;
 (*str;  raise Test_failure;; *)

