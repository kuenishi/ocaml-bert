
let print_termfile filename = 
  let buflen = 1024 in
  let buffer = String.create buflen in
  let load_file filename = 
    let rec load channel data = 
      try 
	begin match input channel buffer 0 buflen with
	  | 0 -> 
	      close_in channel;
	      data;
	  | bytes_read -> 
	      load channel (String.concat "" [(String.sub buffer 0 bytes_read); data]);
	end
      with
	| End_of_file ->  
	    close_in channel;
	    data
    in
    let fch = open_in_bin filename in (* opening file channel *)
      load fch ""
  in
  let rec decode_all bin = 
    begin match Bert.decode_binary bin with
      | Bert.Ok(term, remain)->
	  Bert.print_erlterm term;
	  print_endline "";
	  if String.length remain > 0 then 
	    decode_all remain
	  else
	    print_endline "==all read.";
      | _->
	  print_endline "==failed in decoding.";
    end in
  let bin = load_file filename in
    Printf.printf "reading %s: \n" filename;
(*    Bert.print_binary_string bin; *)
    decode_all bin;;

let _ = 
  let argv = Array.to_list Sys.argv in
    print_string "==";
    match List.tl argv with
      | [] -> 
	  print_string "no files specified - ";
	  [print_termfile "termwriter.bert"];
      | list -> 
	  List.map print_termfile list;;
