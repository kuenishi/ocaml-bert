%%% File    : termwriter.erl
%%% Author  : UENISHI Kota <kuenishi@gmail.com>
%%% Description : a tool for BERT format file creation.
%%% Created : 27 Mar 2010 by UENISHI Kota <kuenishi@gmail.com>

-module(termwriter).
-compile(export_all).

main([])->
    {ok,Terms}=file:consult("termwriter.eterm"),
    io:format("written: ~p~n", [Terms]),
    {ok, IoDevice}=file:open("termwriter.bert", [write,raw]),
    write_(IoDevice, Terms),
    file:close(IoDevice),
    test().

write_(_, [])-> ok;
write_(IoDevice, [Term|Terms])->
    ok=file:write(IoDevice, term_to_binary(Term)),
    write_(IoDevice, Terms).

test()->
    {ok, IoDevice}=file:open("termwriter.bert", [read,raw, binary]),
    {ok, Bin} = read_(IoDevice, <<>>),
    io:format("read: ~p~n", [Bin]),
    file:close(IoDevice).
    
read_(IoDevice, Buffer)->
    case file:read(IoDevice, 1024) of
	{ok, Data}->
	    read_(IoDevice, <<Buffer/binary, Data/binary>>);
	eof -> {ok, binary_to_term(Buffer)};
	Other -> Other 
    end.
