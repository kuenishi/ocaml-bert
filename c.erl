-module(c).

-compile(export_all).

main([])->
    {ok,S}=gen_tcp:connect(localhost,10009,[]),
    gen_tcp:send(S, term_to_binary({some_atom, 123, 12.23, "hogehoge"})),
    case gen_tcp:recv(S,0,1024) of
	{ok, Packet}->
	    erlang:display(binary_to_term(Packet))
    end,
    gen_tcp:close(S).
  
		 
